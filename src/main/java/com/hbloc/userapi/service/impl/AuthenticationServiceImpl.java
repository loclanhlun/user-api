package com.hbloc.userapi.service.impl;

import com.hbloc.userapi.configs.JwtService;
import com.hbloc.userapi.entity.*;
import com.hbloc.userapi.repository.*;
import com.hbloc.userapi.request.AuthenticationRequest;
import com.hbloc.userapi.request.RegisterRequest;
import com.hbloc.userapi.response.AuthenticationResponse;
import com.hbloc.userapi.response.BaseResponse;
import com.hbloc.userapi.service.IAuthenticationService;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class AuthenticationServiceImpl implements IAuthenticationService {

    private final UserRepository userRepository;
    private final UserStatusRepository userStatusRepository;
    private final UserInfoRepository userInfoRepository;
    private final UserRoleRepository userRoleRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;
    private final AuthenticationManager authenticationManager;
    private final JwtService jwtService;

    public AuthenticationServiceImpl(UserRepository userRepository, UserStatusRepository userStatusRepository, UserInfoRepository userInfoRepository, UserRoleRepository userRoleRepository, RoleRepository roleRepository, PasswordEncoder passwordEncoder, AuthenticationManager authenticationManager, JwtService jwtService) {
        this.userRepository = userRepository;
        this.userStatusRepository = userStatusRepository;
        this.userInfoRepository = userInfoRepository;
        this.userRoleRepository = userRoleRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
        this.authenticationManager = authenticationManager;
        this.jwtService = jwtService;
    }

    @Override
    @Transactional
    public BaseResponse register(RegisterRequest request) throws Exception {
        Optional<UserEntity> userEntity = userRepository.findByEmail(request.getEmail());
        var response = new BaseResponse();
        if (userEntity.isPresent()) {
            response.setResultCode(999);
            response.setResultDescription("Email is already exist!");
            return response;
        }

        if (!request.getPassword().equals(request.getConfirmPassword())) {
            response.setResultCode(998);
            response.setResultDescription("Email is already exist!");
            return response;
        }

        var userStatusEntity = userStatusRepository.findByCode("ACTIVE").orElseThrow(() -> new Exception("User Active is not exist"));

        var user = getUser(request, userStatusEntity);

        userRepository.save(user);

        var userInfo = getUserInfoEntity(request, user);
        userInfoRepository.save(userInfo);

        var role = roleRepository.findByCode("ROLE_USER").orElseThrow(() -> new Exception("Role is not exist!"));

        var userRole = UserRoleEntity.builder()
                .role(role)
                .user(user)
                .build();

        userRoleRepository.save(userRole);

        response.setResultCode(0);
        response.setResultDescription("Success");
        return response;
    }

    private UserEntity getUser(RegisterRequest request, UserStatusEntity userStatusEntity) {
        var user = UserEntity
                .builder()
                .email(request.getEmail())
                .password(passwordEncoder.encode(request.getPassword()))
                .userStatus(userStatusEntity)
                .build();
        return user;
    }

    private UserInfoEntity getUserInfoEntity(RegisterRequest request, UserEntity user) {
        var userInfo = UserInfoEntity.builder()
                .user(user)
                .phoneNumber(request.getPhoneNumber())
                .address(request.getAddress())
                .districts(request.getDistricts())
                .ward(request.getWard())
                .province(request.getProvince())
                .age(request.getAge())
                .firstName(request.getFirstName())
                .lastName(request.getLastName())
                .logo(request.getLogo())
                .gender(request.getGender())
                .createdAt(new Date())
                .build();
        return userInfo;
    }

    @Override
    @Transactional
    public AuthenticationResponse authenticate(AuthenticationRequest request) throws Exception {
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(request.getEmail(), request.getPassword()));
        var user = userRepository.findByEmail(request.getEmail()).orElseThrow(() -> new Exception("Account is not exist!"));
        var jwtToken = jwtService.generateToken(user);
        return AuthenticationResponse.builder()
                .resultCode(0)
                .resultDescription("Success")
                .sessionToken(jwtToken)
                .build();
    }
}
