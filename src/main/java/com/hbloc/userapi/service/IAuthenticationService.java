package com.hbloc.userapi.service;

import com.hbloc.userapi.request.AuthenticationRequest;
import com.hbloc.userapi.request.RegisterRequest;
import com.hbloc.userapi.response.AuthenticationResponse;
import com.hbloc.userapi.response.BaseResponse;

public interface IAuthenticationService {
    BaseResponse register(RegisterRequest request) throws Exception;
    AuthenticationResponse authenticate(AuthenticationRequest request) throws Exception;
}
