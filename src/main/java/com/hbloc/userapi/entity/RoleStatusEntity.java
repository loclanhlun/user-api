package com.hbloc.userapi.entity;

import jakarta.persistence.*;
import lombok.*;

import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "`role_status`")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RoleStatusEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String code;

    private String description;

    @OneToMany(mappedBy = "roleStatus")
    private Set<RoleEntity> roles;

    private Date createdAt;

    private Date updatedAt;

    @PrePersist
    public void prePersist() {
        this.createdAt = new Date();
    }

    @PreUpdate
    public void preUpdate() {
        this.updatedAt = new Date();
    }
}
