package com.hbloc.userapi.entity;

import jakarta.persistence.*;
import lombok.*;

import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "`role`")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RoleEntity {
    @Id
    private int id;

    private String code;

    private String description;

    @ManyToOne
    @JoinColumn(name = "role_status_id", nullable = false)
    private RoleStatusEntity roleStatus;

    @OneToMany(mappedBy = "role")
    private Set<UserRoleEntity> userRoles;

    private Date createdAt;

    private Date updatedAt;

    @PrePersist
    public void prePersist() {
        this.createdAt = new Date();
    }

    @PreUpdate
    public void preUpdate() {
        this.updatedAt = new Date();
    }
}
