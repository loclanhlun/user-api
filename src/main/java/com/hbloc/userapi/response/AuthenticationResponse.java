package com.hbloc.userapi.response;

import lombok.*;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class AuthenticationResponse extends BaseResponse{
    private String sessionToken;
}
