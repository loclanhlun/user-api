package com.hbloc.userapi.api;

import com.hbloc.userapi.request.AuthenticationRequest;
import com.hbloc.userapi.request.RegisterRequest;
import com.hbloc.userapi.response.AuthenticationResponse;
import com.hbloc.userapi.response.BaseResponse;
import com.hbloc.userapi.service.IAuthenticationService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1/auth")
public class AuthenticationController {
    private final IAuthenticationService authenticationService;

    public AuthenticationController(IAuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @PostMapping(value = "register")
    public ResponseEntity<?> register(@RequestBody RegisterRequest request) {
        BaseResponse response = new BaseResponse();
        try {
            response = authenticationService.register(request);
        } catch (Exception ex) {
            response.setResultCode(9999);
            response.setResultDescription(ex.getMessage());
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping(value = "/authenticate")
    public ResponseEntity<?> authenticate(@RequestBody AuthenticationRequest request) {
        AuthenticationResponse response = new AuthenticationResponse();
        try {
            response = authenticationService.authenticate(request);
        } catch (Exception ex) {
            response.setResultCode(9999);
            response.setResultDescription(ex.getMessage());
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
