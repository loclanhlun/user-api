package com.hbloc.userapi.repository;

import com.hbloc.userapi.entity.RoleStatusEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleStatusRepository extends JpaRepository<RoleStatusEntity, Integer> {
}
